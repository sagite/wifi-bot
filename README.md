##Overview
   This project aims at autonomous robot able to have accurate information about the wifi of certain locations.

##Technologies
   * Python
   * MongoDB
   *  ROS


##Setup
   * Install ROS on your workstation
   * Install ROs on your robot (we used Turtlrbot)
   * Install the python libs: `sudo pip install -r dependencies.txt`

##Run
   1. Download the repository  
   2. Insert the map.xml and places.yaml into a data repository inside the project's repository
   3. Execute `python Interface.py [data repository name]`
      for example: if map.xml and places.xml sits inside data execute: `python interface.py data`

##Dependencies:

pyspeedtest,pymongo,pyros