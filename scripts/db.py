#!/usr/bin/env python
# DB

import pymongo
import bson

MONGODB_URI1 = 'mongodb://sagitt:turtlebot16@ds059145.mlab.com:59145/example'
MONGODB_URI2 = 'mongodb://yarin:turtlebot16@ds031865.mlab.com:31865/turtlebot'


#TODO create and add actual local_db and db
#example = local_db
#turtlebot = db 

class DB:
	''' 
	this class maintain the cloud DB
	'''
	def __init__(self):

		
		self.client_db=pymongo.MongoClient(MONGODB_URI2)
		self.db=self.client_db.get_default_database()
		self.initialize_local_db()
	
	
	def initialize_local_db(self):
		''' 
		this func initialize the mongodb that sits on the turtlebot
		'''
		self.client_local_db=pymongo.MongoClient(MONGODB_URI1)
		self.local_db=self.client_local_db.get_default_database()


	def update_temp_db(self,features):
		for feature in features:
			new_data = [
			{
				'place' : feature[0],
				'time' : feature[1],
				'day' : feature[2],
				'ping' : feature[3],
				'upload' : feature[4],
				'download':feature[5],
			}]

			localdata = self.local_db['local_data']
			localdata.insert(new_data)
		self.client_local_db.close()



	def update_db(self):


		db_data=[]
		for doc in self.local_db['local_data'].find():
			db_data.append(doc)
		
		for doc in db_data:
			if '_id' in doc:
				del doc['_id']

		dbdata=self.db['data']
		dbdata.insert(db_data)
		self.local_db.drop_collection('local_data')
		



	def find_in_db(self,fdate,fday,ftime):
		return db.find({'date':fdate},{'day':fday},{'time':ftime})

